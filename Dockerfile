FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

ADD ca.repo /etc/yum.repos.d/ca.repo
ADD wlcg-el9.repo /etc/yum.repos.d/wlcg-el9.repo
ADD RPM-GPG-KEY-wlcg /etc/pki/rpm-gpg/RPM-GPG-KEY-wlcg
ADD GPG-KEY-EUGridPMA-RPM-3 /etc/pki/rpm-gpg/GPG-KEY-EUGridPMA-RPM-3

RUN dnf install -y https://linuxsoft.cern.ch/wlcg/el9/x86_64/wlcg-repo-1.0.0-1.el9.noarch.rpm && \
    dnf install -y dnf-plugins-core epel-release && \
    dnf config-manager --add-repo "http://linuxsoft.cern.ch/mirror/repository.egi.eu/sw/production/cas/1/current/" && \
    dnf install -y \
    libffi-devel openssl-devel \
    python3-pip gfal2-all gfal2-util \
    CERN-CA-certs voms-clients-java \
    ca-policy-egi-core wlcg-voms-cms wlcg-iam-vomses-cms && \
    dnf clean all && \
    rm -rf /var/cache/yum
