# grid-proxy-test

Demo to obtain a VOMS grid proxy in a container using secrets.

Variables used:

- `GRID_PASSWORD`: password for the grid certificate
- `GRID_USERCERT`: grid user certificate (`usercert.pem`)
- `GRID_USERKEY`: grid user key (`userkey.pem`)

Mind that certificate and key need to be encoded using `base64`:

```shell
cat ~/.globus/usercert.pem | base64 | pbcopy
cat ~/.globus/userkey.pem | base64 | pbcopy
```
